FROM node:14.15.4-alpine

WORKDIR /usr/src/app

ADD . /usr/src/app
RUN npm config set registry http://registry.npmjs.org
#RUN npm install -g node-gyp node-pre-gyp
#RUN echo http://mirror.yandex.ru/mirrors/alpine/v3.5/main > /etc/apk/repositories;
#RUN echo http://mirror.yandex.ru/mirrors/alpine/v3.5/community >> /etc/apk/repositories
# RUN apk update
RUN npm i -g @nestjs/cli
RUN npm install
RUN npm run build
RUN npm config set user 0
RUN npm config set unsafe-perm true
# Set time zone
RUN apk add --update tzdata
RUN cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime
RUN echo "Asia/Bangkok" > /etc/timezone
RUN apk del tzdata
RUN apk add --update tzdata

RUN apk update && apk add nano

# prod
# env
ENV ENV=prod
EXPOSE 3000

CMD [ "npm", "run", "start:prod" ]
