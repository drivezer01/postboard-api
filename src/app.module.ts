import { UploadsModule } from './modules/uploads/uploads.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import * as dotenv from 'dotenv';
import { APP_FILTER } from '@nestjs/core';
import { HttpExceptionFilter } from './shared/http-exception.filter';
import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';
import { PostsModule } from './modules/posts/posts.module';
import { PostImagesModule } from './modules/post-images/post-images.module';
import { ChatsModule } from './modules/chats/chats.module';

dotenv.config();
const options = {
  user:
    process.env.ENV === 'prod'
      ? process.env.PROD_DB_USER
      : process.env.DEV_DB_USER,
  pass:
    process.env.ENV === 'prod'
      ? process.env.PROD_DB_PASS
      : process.env.DEV_DB_PASS,
  useNewUrlParser: true,
  // useCreateIndex: true,
  useUnifiedTopology: true,
};

@Module({
  imports: [
    MongooseModule.forRoot(
      process.env.ENV === 'prod'
        ? process.env.PROD_DB_URL
        : process.env.DEV_DB_URL,
      options,
    ),
    UsersModule,
    AuthModule,
    UploadsModule,
    PostsModule,
    PostImagesModule,
    ChatsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    }
  ],
})
export class AppModule {}
