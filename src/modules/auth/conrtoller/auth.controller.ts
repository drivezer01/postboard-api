import { Controller, Post, Request } from '@nestjs/common';
import { AuthService } from '../service/auth.service';

@Controller('api/auth')
export class AuthController {

    constructor(private _auth: AuthService) {}
    
    @Post('login')
    async login(@Request() req) {
        return this._auth.login(req.body);
    }
}
