import { UsersService } from './../../users/service/users.service';
import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
    constructor(
        @Inject(forwardRef(() => UsersService))
        private _users: UsersService,
        private _jwtService: JwtService
    ) {}

    BCRYPT_HASH_ROUND = 8;

    async compareHash(password: string, hash: string): Promise<boolean> {
        return await bcrypt.compareSync(password, hash);
    }

    async hashPassword(password: string): Promise<string> {
        return await bcrypt.hash(password, this.BCRYPT_HASH_ROUND);
    }

    async login(data: any) {
        const user = await this._users.findByUserEmail(data.user_email);
        if (!user) {
            throw new HttpException('ไม่พบ Email ในระบบ', HttpStatus.FORBIDDEN);
        }

        if (!await this.compareHash(data.user_password, user.user_password)) {
            throw new HttpException('กรุณาตรวจ Email กับ Password อีกครั้ง', HttpStatus.FORBIDDEN);
        }
        
        const payload = { user_id: user._id, iat: new Date().getTime() };
        return {
            access_token: this._jwtService.sign(payload),
        };
    }
}