import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from '../users/users.module';
import { ChatsController } from './controller/chats.controller';
import { ChatSchema } from './schema/chat.schema';
import { ChatsService } from './service/chats.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Chats', schema: ChatSchema}]),
    UsersModule
  ],
  controllers: [ChatsController],
  providers: [ChatsService],
  exports: [ChatsService]
})
export class ChatsModule {}
