import { Body, Controller, Delete, Get, Param, Post, Put, Req } from '@nestjs/common';
import { ChatModel } from '../model/chat.model';
import { ChatsService } from '../service/chats.service';

@Controller('api/chats')
export class ChatsController {
    constructor(
        private _chats: ChatsService,
    ) {}

    @Get()
    async findAll(@Req() req) {
        return await this._chats.findAll(req.query);
    }

    @Get(':chatId')
    async findOne(@Param('chatId') chatId: string) {
        return await this._chats.findOne(chatId);
    }

    @Get('/actions/chat-list/:userId')
    async getChatList(@Param('userId') userId: string) {
        return await this._chats.getChatList(userId);
    }

    @Post()
    async create(@Body() data: ChatModel) {
        return await this._chats.create(data);
    }

    @Put(':chatId')
    async update(@Param('chatId') chatId: string, @Body() data: ChatModel) {
        return await this._chats.update(chatId, data);
    }

    @Delete(':chatId')
    async delete(@Param('chatId') chatId: string) {
        return await this._chats.delete(chatId);
    }
}
