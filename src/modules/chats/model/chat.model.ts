import { Document } from 'mongoose';

export class ChatModel extends Document {
    chat_room: string;
    chat_sender_id: string;
    chat_received_id: string;
    chat_text: string;
    created_at?: Date;
    updated_at?: Date;
}