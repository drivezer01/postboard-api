import { Schema } from "mongoose";

export const ChatSchema = new Schema({
    chat_room: String,
    chat_sender_id: String,
    chat_received_id: String,
    chat_text: String,
    created_at: { type: Date, default: Date.now() },
    updated_at: { type: Date, default: Date.now() }
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});