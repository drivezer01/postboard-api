import { OptionParams } from './../../../shared/options-params.interface';
import { UsersService } from './../../users/service/users.service';
import { MongoCRUD } from './../../../shared/mongo-curd';
import { ChatModel } from './../model/chat.model';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import * as _ from 'lodash';

@Injectable()
export class ChatsService extends MongoCRUD<ChatModel> {

    public serviceName = 'ChatsService';

    constructor(
        @InjectModel('Chats') public model: Model<ChatModel>,
        private readonly _users: UsersService
    ) {
        super(model);
    }

    async findAll(options?: OptionParams) {
        try {
            this.search = {};
            if (!_.isEmpty(options.s)) {
                this.search = await this.getMatchFilter(null, options);
            }

            const result = await this._model.aggregate([
                { $match: this.search }
            ]);
            return {
                data: result,
                total: result.length
            }
        } catch (ex) {
            throw new HttpException(`Exception findAll ${this.modelName} ====> ${ex}`, HttpStatus.BAD_REQUEST,
            );
        }
    }

    async getChatList(userId: string) {
        try{
            const result = await this.model.aggregate([
                {
                    $match: {
                        $or: [
                            { chat_sender_id: userId },
                            { chat_received_id: userId }
                        ]
                    }
                },
                {
                    $group: {
                        _id: '$chat_room',
                        chat_sender_id: { $last: '$chat_sender_id' },
                        chat_received_id: { $last: '$chat_received_id' },
                        chat_text: { $last: '$chat_text'}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        chat_room: '$_id',
                        chat_sender_id: '$chat_sender_id',
                        chat_received_id: '$chat_received_id',
                        chat_text: 1,
                        
                    }
                }
            ]);
            
            const newArray = [];
            for (const item of result) {
                const resSender = await this._users.findOne(item.chat_sender_id);
                const resReceive = await this._users.findOne(item.chat_received_id);
                newArray.push({
                    chat_room: item.chat_room,
                    chat_text: item.chat_text,
                    chat_sender: resSender,
                    chat_received: resReceive
                });
            }
            return { data: newArray, total: newArray.length };
        } catch (ex) {
            throw new HttpException(`Exception getChatList ${this.modelName} ====> ${ex}`, HttpStatus.BAD_REQUEST);
        }
    }

    async create(data: ChatModel) {
        try {
            if (data.chat_room === 'null') {
                data.chat_room = Math.floor(Math.random() * 100000000).toString();
            }
            
            return await this._model.create(data);
        } catch (ex) {
            throw new HttpException(`Exception create ${this.modelName} ====> ${ex}`, HttpStatus.BAD_REQUEST);
        }
    }
}
