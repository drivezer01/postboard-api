import { Body, Controller, Delete, Get, Param, Post, Put, Req } from '@nestjs/common';
import { PostImageModel } from '../model/post-image.model';
import { PostImagesService } from '../service/post-images.service';

@Controller('api/post-images')
export class PostImagesController {
    constructor(
        private _postImages: PostImagesService,
    ) {}

    @Get()
    async findAll(@Req() req) {
        return await this._postImages.findAll(req.query);
    }

    @Get(':imageId')
    async findOne(@Param('imageId') imageId: string) {
        return await this._postImages.findOne(imageId);
    }

    @Post()
    async create(@Body() data: PostImageModel) {
        return await this._postImages.create(data);
    }

    @Put(':imageId')
    async update(@Param('imageId') imageId: string, @Body() data: PostImageModel) {
        return await this._postImages.update(imageId, data);
    }

    @Delete(':imageId')
    async delete(@Param('imageId') imageId: string) {
        return await this._postImages.delete(imageId);
    }
}
