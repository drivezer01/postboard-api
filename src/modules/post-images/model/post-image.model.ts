import { Document } from 'mongoose';

export class PostImageModel extends Document {
    image_post_id: string;
    image_thumbnail: string;
    created_at?: Date;
    updated_at?: Date;
}