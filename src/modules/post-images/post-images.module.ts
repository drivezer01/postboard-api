import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PostImagesController } from './controller/post-images.controller';
import { PostImageSchema } from './schema/post-image.schema';
import { PostImagesService } from './service/post-images.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Post-Images', schema: PostImageSchema}]),
  ],
  controllers: [PostImagesController],
  providers: [PostImagesService],
  exports: [PostImagesService]
})
export class PostImagesModule {}
