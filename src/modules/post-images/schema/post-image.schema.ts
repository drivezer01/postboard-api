import { Schema } from "mongoose";

export const PostImageSchema = new Schema({
    image_post_id: String,
    image_thumbnail: String,
    created_at: { type: Date, default: Date.now() },
    updated_at: { type: Date, default: Date.now() }
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});