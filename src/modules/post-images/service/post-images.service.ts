import { MongoCRUD } from './../../../shared/mongo-curd';
import { PostImageModel } from './../model/post-image.model';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class PostImagesService extends MongoCRUD<PostImageModel> {

    public serviceName = 'PostImagesService';

    constructor(
        @InjectModel('Post-Images') public model: Model<PostImageModel>,
    ) {
        super(model);
    }
}
