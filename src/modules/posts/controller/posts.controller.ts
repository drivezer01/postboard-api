import { Body, Controller, Delete, Get, Param, Post, Put, Req } from '@nestjs/common';
import { PostModel } from '../model/post.model';
import { PostsService } from '../service/posts.service';

@Controller('api/posts')
export class PostsController {
    constructor(
        private _posts: PostsService,
    ) {}

    @Get()
    async findAll(@Req() req) {
        return await this._posts.findAll(req.query);
    }

    @Get(':postId')
    async findOne(@Param('postId') postId: string) {
        return await this._posts.findOne(postId);
    }

    @Post()
    async create(@Body() data: PostModel) {
        return await this._posts.create(data);
    }

    @Put(':postId')
    async update(@Param('postId') postId: string, @Body() data: PostModel) {
        return await this._posts.update(postId, data);
    }

    @Delete(':postId')
    async delete(@Param('postId') postId: string) {
        return await this._posts.delete(postId);
    }
}
