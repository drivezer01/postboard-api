import { Document } from 'mongoose';

export class PostModel extends Document {
    post_user_id: string;
    post_car_detail: string;
    post_location: string;
    post_detail: string;
    post_phone: string;
    post_line: string;
    post_facebook: string;
    post_other: string;
    created_at?: Date;
    updated_at?: Date;
}