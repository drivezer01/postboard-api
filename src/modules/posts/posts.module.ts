import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from '../users/users.module';
import { PostsController } from './controller/posts.controller';
import { PostSchema } from './schema/post.schema';
import { PostsService } from './service/posts.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Posts', schema: PostSchema}]),
    UsersModule
  ],
  controllers: [PostsController],
  providers: [PostsService],
  exports: [PostsService]
})
export class PostsModule {}
