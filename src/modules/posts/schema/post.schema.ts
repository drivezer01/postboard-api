import { Schema } from "mongoose";

export const PostSchema = new Schema({
    post_user_id: String,
    post_car_detail: String,
    post_location: String,
    post_detail: String,
    post_phone: String,
    post_line: String,
    post_facebook: String,
    post_other: String,
    created_at: { type: Date, default: Date.now() },
    updated_at: { type: Date, default: Date.now() }
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});