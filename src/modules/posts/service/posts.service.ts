import { UsersService } from './../../users/service/users.service';
import { OptionParams } from './../../../shared/options-params.interface';
import { MongoCRUD } from './../../../shared/mongo-curd';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { PostModel } from '../model/post.model';
import { InjectModel } from '@nestjs/mongoose';
import * as _ from 'lodash';

@Injectable()
export class PostsService extends MongoCRUD<PostModel> {

    public serviceName = 'PostsService';

    constructor(
        @InjectModel('Posts') public model: Model<PostModel>,
        private readonly _user: UsersService
    ) {
        super(model);
    }

    async findAll(options?: OptionParams) {
        try {
            this.search = {};
            if (!_.isEmpty(options.s)) {
                this.search = await this.getMatchFilter(null, options);
            }

            const newArray = [];
            const result = await this._model.find(this.search).exec();
            for (const item of result) {
                const user = await this._user.findOne(item.post_user_id);
                newArray.push({
                    _id: item._id,
                    post_user_id: item.post_user_id,
                    post_car_detail: item.post_car_detail,
                    post_location: item.post_location,
                    post_detail: item.post_detail,
                    post_phone: item.post_phone,
                    post_line: item.post_line,
                    post_facebook: item.post_facebook,
                    post_other: item.post_other,
                    user_email: user.user_email,
                    user_firstname: user.user_firstname,
                    user_lastname: user.user_lastname,
                    user_thumbnail: user.user_thumbnail,
                })
            }
            return {
                data: newArray,
                total: newArray.length
            }
        } catch (ex) {
            throw new HttpException(`Exception findAll ${this.modelName} ====> ${ex}`, HttpStatus.BAD_REQUEST,
            );
        }
    }
}
