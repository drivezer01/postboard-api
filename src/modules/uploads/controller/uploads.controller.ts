import { Controller, Get, HttpException, HttpStatus, Param, Post, Req, Res, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Response, Request } from 'express';
import { diskStorage } from 'multer';
import * as mkdirp from 'mkdirp';
import { extname } from 'path';

@Controller('api/uploads')
export class UploadsController {
    constructor() {}

    @Get(':types/:file_name')
    async seeUploadedFile(@Req() req: any, @Param('file_name') fileName, @Param('types') types, @Res() res: Response) {
        try {
            let dir = '';
            switch (types) {
                case 'profile':
                    // uploads/profile/image-profile-1635757394255.JPG
                    dir = `uploads/profiles`;
                    break;
                case 'post':
                    dir = `uploads/posts`;
                    break;
            }
            return res.sendFile(fileName, { root: dir }, (err) => {
                if (err) {
                    return res.sendFile('placeholder-image.png', { root: 'assets/images' });
                }
            });
        } catch (e) {
            throw new HttpException(`UploadController ${e}`, 400);
        }
    }

    @Post('post')
    @UseInterceptors(FileInterceptor('image', {
        storage: diskStorage({
            async destination(req: Request, file, cb) {
                const dir = `./uploads/posts/`;
                try {
                    mkdirp.sync(dir);
                    cb(null, dir);
                } catch (e) {
                    throw new HttpException(`Make directory ${e}`, 400);
                }
            },
            filename: (req: Request, file, cb) => {
                cb(null, `${file.fieldname + '-post-' + Date.now()}${extname(file.originalname)}`);
            },
        }),
    }))
    async uploadImagesPost(@UploadedFiles() images, @Res() res) {
        try {
            return await res.status(HttpStatus.OK).json({ upload_name: res.req.file.filename });
        } catch (e) {
            console.log(`UseInterceptors FileInterceptor uploadImagesPost ${e}`);
            throw new HttpException(`UseInterceptors FileInterceptor uploadImagesPost ${e}`, 400);
        }
    }

    @Post('profile')
    @UseInterceptors(FileInterceptor('image', {
        storage: diskStorage({
            async destination(req: Request, file, cb) {
                const dir = `./uploads/profiles/`;
                try {
                    mkdirp.sync(dir);
                    cb(null, dir);
                } catch (e) {
                    throw new HttpException(`Make directory ${e}`, 400);
                }
            },
            filename: (req: Request, file, cb) => {
                cb(null, `${file.fieldname + '-profile-' + Date.now()}${extname(file.originalname)}`);
            },
        }),
    }))
    async uploadImagesProfile(@UploadedFiles() images, @Res() res) {
        try {
            return await res.status(HttpStatus.OK).json({ upload_name: res.req.file.filename });
        } catch (e) {
            console.log(`UseInterceptors FileInterceptor uploadImagesProfile ${e}`);
            throw new HttpException(`UseInterceptors FileInterceptor uploadImagesProfile ${e}`, 400);
        }
    }
}
