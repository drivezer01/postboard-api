import { Body, Controller, Delete, Get, Param, Post, Put, Req } from '@nestjs/common';
import { UserModel } from '../model/user.model';
import { UsersService } from '../service/users.service';

@Controller('api/users')
export class UsersController {
    constructor(
        private _users: UsersService,
    ) {}

    @Get()
    async findAll(@Req() req) {
        return await this._users.findAll(req.query);
    }

    @Get(':userId')
    async findOne(@Param('userId') userId: string) {
        return await this._users.findOne(userId);
    }

    @Post()
    async create(@Body() data: UserModel) {
        return await this._users.create(data);
    }

    @Put(':userId')
    async update(@Param('userId') userId: string, @Body() data: UserModel) {
        return await this._users.update(userId, data);
    }

    @Delete(':userId')
    async delete(@Param('userId') userId: string) {
        return await this._users.delete(userId);
    }
}
