import { Document } from 'mongoose';

export class UserModel extends Document {
    user_email: string;
    user_password: string;
    user_firstname: string;
    user_lastname: string;
    user_thumbnail: string;
    created_at?: Date;
    updated_at?: Date;
}