import { Schema } from "mongoose";

export const UserSchema = new Schema({
    user_email: String,
    user_password: String,
    user_firstname: String,
    user_lastname: String,
    user_thumbnail: String,
    created_at: { type: Date, default: Date.now() },
    updated_at: { type: Date, default: Date.now() }
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});