import { AuthService } from './../../auth/service/auth.service';
import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserModel } from '../model/user.model';
import { MongoCRUD } from './../../../shared/mongo-curd';

@Injectable()
export class UsersService extends MongoCRUD<UserModel> {
    
    public serviceName = 'UsersService';
    
    constructor(
        @InjectModel('Users') public model: Model<UserModel>,
        @Inject(forwardRef(() => AuthService))
        private readonly _auth: AuthService,
    ) {
        super(model);
    }

    async findByUserEmail(user_email: string) {
        try {
            return await this._model.findOne({ user_email }).exec();
        } catch (ex) {
            throw new HttpException(`Exception findByUserEmail ${this.serviceName} ====> ${ex}`, HttpStatus.BAD_REQUEST);
        }
    }

    async create(data: UserModel) {
        try {
            data.user_email = data.user_email.trim();
            data.user_password = data.user_password.trim();
            data.user_password = await this._auth.hashPassword(data.user_password);
            return await this._model.create(data);
        } catch (ex) {
            throw new HttpException(`Exception create ${this.serviceName} ====> ${ex}`, HttpStatus.BAD_REQUEST);
        }
    }

    async update(userId: string, data: UserModel) {
        try {
            const user = await this.findOne(userId);
            if (data.user_email !== undefined) {
                data.user_email = data.user_email.trim();
            }
            if (user.user_password !== data.user_password && data.user_password !== undefined) {
                if (data.user_password !== '') {
                    data.user_password = data.user_password.trim();
                    data.user_password = await this._auth.hashPassword(data.user_password);
                } else {
                    delete data.user_password;
                }
            }
            return await this._model.findByIdAndUpdate({ _id: userId }, data, { new: true, upsert: true });
        } catch (ex) {
            throw new HttpException(`Exception update ${this.modelName} ====> ${ex}`, HttpStatus.BAD_REQUEST);
        }
    }


}
