import { HttpException, HttpStatus } from "@nestjs/common";
import * as _ from 'lodash';
import { Model } from "mongoose";
import { OptionParams } from "./options-params.interface";

export class MongoCRUD<T> {
    public modelName: string;
    protected _model: Model<T>;
    operatorGreaterLess = ['$gte', '$gt', '$lte', '$lt', '$ne', '$regex', '$or', '$and'];
    search = {};

    constructor(public model: Model<T>) {
        this.modelName = model.modelName;
        this._model = model;
    }

    async findAll(options?: OptionParams) {
        try {
            this.search = {};
            if (!_.isEmpty(options.s)) {
                this.search = await this.getMatchFilter(null, options);
            }

            const result = await this._model.find(this.search).exec();
            return {
                data: result,
                total: result.length
            }
        } catch (ex) {
            throw new HttpException(`Exception findAll ${this.modelName} ====> ${ex}`, HttpStatus.BAD_REQUEST,
            );
        }
    }

    async findOne(userId: string) {
        try {
            return await this._model.findById(userId).exec();
        } catch (ex) {
            throw new HttpException(`Exception findOne ${this.modelName} ====> ${ex}`, HttpStatus.BAD_REQUEST,
            );
        }
    }

    async create(data: T) {
        try {
            return await this._model.create(data);
        } catch (ex) {
            throw new HttpException(`Exception create ${this.modelName} ====> ${ex}`, HttpStatus.BAD_REQUEST);
        }
    }

    async update(id: string | any, data: T) {
        try {
            return await this._model.findByIdAndUpdate({ _id: id }, data, { new: true, upsert: true });
        } catch (ex) {
            throw new HttpException(`Exception update ${this.modelName} ====> ${ex}`, HttpStatus.BAD_REQUEST);
        }
    }

    async delete(id: string | any) {
        try {
            return await this._model.findByIdAndDelete({ _id: id });
        } catch (ex) {
            throw new HttpException(`Exception delete ${this.modelName} ====> ${ex}`, HttpStatus.BAD_REQUEST);
        }
    }

    async getMatchFilter(prefix: string, options: OptionParams): Promise<any> {
        let isFullText = false;
        if (_.isEmpty(options)) return {};
        if (!_.isEmpty(options.is_full_text)) {
            isFullText = (/true/i).test(options.is_full_text) === true ? true : false
        }
        if (!_.isEmpty(options.s)) {
            try {
                let search = JSON.parse(options.s);
                const operator = Object.keys(search)[0];
                search[operator] = await Promise.all(search[operator].map((element: any) => {
                    const key = Object.keys(element)[0];
                    if (element[key] === '') return;
                    // console.log(`${key} ====> `, element[key], ', ', typeof element[key]);
                    switch (typeof element[key]) {
                        case 'object':
                            for (const operator of Object.keys(element[key])) {
                                // console.log('Input ====> ', element[key][operator]);
                                if (!this.invalidOperator(operator) && typeof Number(operator) !== 'number') {
                                    throw new HttpException(`Unknow operator: ${operator}`, HttpStatus.BAD_REQUEST);
                                } else {
                                    if (element[key][operator].length === 10 || element[key][operator].length === 19) {
                                        element[key][operator] = this.getDateTimeUTC(element[key][operator], operator);
                                    } else {
                                        // Catch when element[key] is { $regex: value }
                                        try {
                                            element[key][operator] = JSON.parse(element[key][operator]);
                                        } catch { }
                                    }
                                }
                                // console.log(`result ====> ${operator}   ${element[key][operator]}`);
                            }
                            return { [`${prefix === null ? '' : prefix + '.'}${key}`]: element[key] }
                        case 'number':
                            return { [`${prefix === null ? '' : prefix + '.'}${key}`]: element[key] };
                        default:
                            return isFullText ?
                                { [`${prefix === null ? '' : prefix + '.'}${key}`]: `${element[key]}` } :
                                { [`${prefix === null ? '' : prefix + '.'}${key}`]: { $regex: element[key] } };
                    }
                }));
                // console.log(search[operator]);
                const searchComplete = { [operator]: [] }
                for (const field of search[operator]) {
                    if (field !== undefined) {
                        searchComplete[operator].push(field);
                    }
                }
                // console.log(searchComplete);
                return searchComplete;
            } catch (ex) {
                console.log(`Exception getMatchFilter ${this.modelName} ====> ${ex}`);
            }
        }
    }

    private invalidOperator(value: string): boolean {
        return this.operatorGreaterLess.includes(value);
    }

    private getDateTimeUTC(date: string, operator: string): Date {
        try {
            if (_.isEmpty(date)) { return; };
            const [searchDate, searchTime] = date.split(' ');
            const [_year, _month, _date] = searchDate.split('-');
            if (searchDate && searchTime) {
                const [searchHour, searchMin, searchSec] = searchTime.split(':');
                return new Date(Number(_year), Number(_month) - 1, Number(_date), Number(searchHour), Number(searchMin), Number(searchSec));
            } else {
                if (operator === '$gte' || operator === '$gt') {
                    return new Date(Number(_year), Number(_month) - 1, Number(_date), 0, 0, 0, 0);
                } else if (operator === '$lte' || operator === '$lt') {
                    return new Date(Number(_year), Number(_month) - 1, Number(_date), 23, 59, 59, 999);
                } else {
                    return new Date(Number(_year), Number(_month) - 1, Number(_date), 0, 0, 0, 0);
                }
            }
        } catch (ex) {
            console.log(`Exception getDateTimeUTC ${this.modelName} ====> ${ex}`);
            return;
        }

    }
}